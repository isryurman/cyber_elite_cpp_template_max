#pragma once

template<typename T>
T max(T t1, T t2, T t3)
{
	if (t1 < t2)
	{
		if (t2 < t3)
		{
			return t3;
		}
		else
		{
			return t2;
		}
	}
	else
	{
		if (t1 < t3)
		{
			return t3;
		}
		else
		{
			return t1;
		}
	}
}