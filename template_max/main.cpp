// Israel yurman
#include <iostream>
#include "template.h"

using namespace std;

int main()
{
	int i1 = 1, i2 = 2, i3 = 3;
	double d1 = 1.1, d2 = 2.2, d3 = 3.3;

	cout << "int max: " << max(i1, i2, i3) << endl;
	cout << "double max: " << max(d1, d2, d3) << endl;

	system("pause");
	return 0;
}